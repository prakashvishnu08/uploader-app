Rails.application.routes.draw do
  mount ActionCable.server => '/cable'
  devise_for :users
  root to: 'dashboards#index'
  resources :sales
  resources :dashboards
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
