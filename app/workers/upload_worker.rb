require 'roo'

class UploadWorker
  include Sidekiq::Worker

  def perform(file_path)
    sleep 3
    xlsx = Roo::Spreadsheet.open(file_path, extension: :xlsx)
    xlsx.each(region: 'Region',
              country: 'Country',
              item: "Item Type",
              order_id: "Order ID",
              unit_sold: "Units Sold",
              unit_price: "Unit Price",
              unit_cost: "Unit Cost",
              total_revenue: "Total Revenue",
              total_cost: "Total Cost",
              total_profit: "Total Profit") do |hash|
      Sale.create!(hash) rescue nil
    end
    ActionCable.server.broadcast "notifications:1", {html: '<div class="alert alert-info alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        Uploading Completed
      </div>'}
  end
end
