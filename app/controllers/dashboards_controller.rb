class DashboardsController < ApplicationController
  def index
    @sales = Sale.order(:item).page params[:page]
  end
end
