class SalesController < ApplicationController
  def new
    @uploaded_file = UploadedFile.new
  end

  def create
    @uploaded_file = UploadedFile.new(file_params)
    @uploaded_file.user = current_user
    if @uploaded_file.save
      flash[:notice] = "Uploading in process"
      redirect_to root_path
    else
      render :new
    end
  end

  private

  def file_params
    params.require(:uploaded_file).permit(:file)
  end

end
