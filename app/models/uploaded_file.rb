class UploadedFile < ApplicationRecord
  belongs_to :user
  has_one_attached :file
  validate :file_size_validation
  after_save :invoke_upload

  def file_size_validation
    errors[:file] << "should be less than 10MB" if self.file.blob.byte_size > 10485760
  end

  def invoke_upload
    ActionCable.server.broadcast "notifications:1", {html: "<div>Started</div>"}
    path =  ActiveStorage::Blob.service.path_for(self.file.key)
    UploadWorker.perform_async(path)
  end
end
