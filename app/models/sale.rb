class Sale < ApplicationRecord
  validates :order_id, uniqueness: true
end
