class CreateSales < ActiveRecord::Migration[5.2]
  def change
    create_table :sales do |t|
      t.string :region
      t.string :country
      t.string :item
      t.string :order_id
      t.integer :unit_sold
      t.decimal :unit_price
      t.decimal :unit_cost
      t.decimal :total_revenue
      t.decimal :total_profit
      t.decimal :total_cost

      t.timestamps
    end
  end
end
