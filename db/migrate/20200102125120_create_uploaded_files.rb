class CreateUploadedFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :uploaded_files do |t|
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
